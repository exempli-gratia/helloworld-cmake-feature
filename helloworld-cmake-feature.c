#include <stdio.h>

int main() {
#ifdef DATABASE
  printf("Hello World from CMake - with database!\n");
#elif NODATABASE
  printf("Hello World from CMake - without database! (default)\n");
#else
  #error We need DATABASE or NODATABASE defined
#endif

  return(0);
}
